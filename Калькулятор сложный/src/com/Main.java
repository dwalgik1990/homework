package com;

import java.util.*;

public class Main {
        private String task;
        private List<Double> numbers = new ArrayList<>();
        private List<Integer> priority = new ArrayList<>();
        private List<Character> actions = new ArrayList<>();

        public void readString() {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Введите выражение");
            String sourceTask = scanner.nextLine();
            task = sourceTask.replaceAll("\\s", "");
        }

        private void getNumbers() {
            String[] numbersArray = task.split("[)(-*/+]");
            List<String> numbersString = new ArrayList<>(Arrays.asList(numbersArray));
            numbersString.removeIf(x -> x.length() == 0);
            for (String item : numbersString) {

//            }  //           if (item.matches("\w")) {
//                System.out.println("Ошибка");
                numbers.add(Double.parseDouble(item));
            }
        }

        private void priorityOperations() {
            int priorityRate = 0;
            for (int i = 0; i < task.length(); i++) {
                if (task.charAt(i) == '(') {
                    priorityRate = priorityRate + 2;
                }
                if (task.charAt(i) == ')') {
                    priorityRate = priorityRate - 2;
                }
                if (task.charAt(i) == '-' || task.charAt(i) == '+') {
                    actions.add(task.charAt(i));
                    priority.add(priorityRate);

                }
                if (task.charAt(i) == '*' || task.charAt(i) == '/') {
                    actions.add(task.charAt(i));
                    priority.add(priorityRate + 1);

                }
            }
        }

        private Double doCalc(Character action, Double one, Double two) {
            Double result = 0.0;
            switch (action) {
                case '/':
                    result = one / two;
                    break;
                case '*':
                    result = one * two;
                    break;
                case '-':
                    result = one - two;
                    break;
                case '+':
                    result = one + two;
                    break;
            }
            return result;
        }

        private Double loop() {
            while (priority.size() > 0) {
                int max = Collections.max(priority);
                int index = priority.indexOf(max);
                Double result = doCalc(actions.get(index), numbers.get(index), numbers.get(index + 1));
                priority.remove(index);
                actions.remove(index);
                numbers.remove(index);
                numbers.set(index, result);

            }
            return null;
        }

        public Double doMagic() {
            getNumbers();
            priorityOperations();
            loop();
            return numbers.get(0);
        }

        public static void main(String[] args) {
            Main Calculator = new Main();
            Calculator.readString();
            Double result = Calculator.doMagic();
            System.out.print(result);
        }
    }


